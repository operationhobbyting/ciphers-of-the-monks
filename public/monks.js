// Magic:
const lines = [[], [0, 0, 1, 0], [1, 0, 1, 1], [0, 1, 1, 1], [0, 0, 0, 1], [0, 0, 1, 1], [1, 0, 0, 1],]
const numbers = [[], [1], [3], [5], [6], [1, 6], [2], [1, 2], [2, 3], [1, 2, 3]]
const moveRight = l => [l[0] + 1, l[1], l[2] + 1, l[3]]
const moveDown = l => [l[0], l[1] + 2, l[2], l[3] + 2]
const flipHorizontal = l => [l[0] ? 0 : 1, l[1], l[2] ? 0 : 1, l[3]]
const flipVertical = l => [l[0] ? 0 : 1, l[1] ? 0 : 1, l[2] ? 0 : 1, l[3] ? 0 : 1]

// SVG:
const svgns = "http://www.w3.org/2000/svg";
let monkSvg;
let lineMap = {}

const setup = () => {
    const monkInput = document.getElementById('monkInput');
    const monkSvgContainer = document.getElementById('monkSvgContainer');
    if (!monkInput || ! monkSvgContainer) return;

    monkInput.addEventListener('input', (e) => onChange(e))
    monkSvg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    monkSvg.setAttribute("width", 'min(60vh, 98vw)');
    monkSvg.setAttribute("viewBox", '-0.1 -0.1 2.2 3.2');
    monkSvg.setAttribute("stroke-width", '0.05');
    monkSvg.setAttribute("stroke", 'black');
    monkSvg.setAttribute("stroke-linecap", 'round');
    monkSvg.appendChild(createLine(1, 0, 1, 3));
    monkSvgContainer.appendChild(monkSvg);
}

const monkFunctions = [
    (i) => i > 10 ? [] : numbers[i].map(j => lines[j]).map(moveRight),
    (i) => i > 10 ? [] : numbers[i].map(j => lines[j]).map(flipHorizontal),
    (i) =>  i > 10 ? [] : numbers[i].map(j => lines[j]).map(flipHorizontal).map(flipVertical).map(moveDown).map(moveRight),
    (i) =>  i > 10 ? [] : numbers[i].map(j => lines[j]).map(flipVertical).map(moveDown),
]

const numberToDigitCoords = num =>
    num.toString().split('').reverse().map(Number).map((n, i) => monkFunctions[i](n))

const createLine = (x1, y1, x2, y2) => {
    const line = document.createElementNS(svgns, "line");
    line.setAttribute("x1", x1);
    line.setAttribute("x2", x2);
    line.setAttribute("y1", y1);
    line.setAttribute("y2", y2);
    return line
}

const addLine = ([x1, y1, x2, y2], nlm) => {
    const key = `l${x1},${y1},${x2},${y2}`
    let l;
    if (lineMap[key])
        l = lineMap[key];
    else {
        l = createLine(x1, y1, x2, y2);
        monkSvg.appendChild(l);
    }
    nlm[key] = l;
}

const renderMonk = (n) => {
    let nlm = {};
    numberToDigitCoords(n).forEach(d => d.forEach(l => addLine(l, nlm)));
    const ok = Object.keys(lineMap);
    const nk = Object.keys(nlm);
    ok.filter(k => !nk.includes(k)).forEach(k => monkSvg.removeChild(lineMap[k]))
    lineMap = nlm;
}


const onChange = (e) => {
    if (!e || !e.target || e.target.value === undefined) return;
    let n = e.target.value || 0;
    let reset = false
    if (n > 9999) {
        reset = true;
        n = 9999;
    } else if (n < 0) {
        reset = true;
        n = 0
    }
    if (reset)
        e.target.value = n;
    renderMonk(n);
}

setup()